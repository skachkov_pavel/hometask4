﻿using Models;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace Client
{
    class Program
    {
        private static readonly HttpClient client = new();
        static void Main(string[] args)
        {
           
        }
        static async Task<List<Project>> GetProjects()
        {
            HttpResponseMessage response = await client.GetAsync("https://localhost:44301/api/Projects");
            string body = await response.Content.ReadAsStringAsync();

            return JsonConvert.DeserializeObject<List<Project>>(body);
        }

        static async Task<Project> GetProjectById(int id)
        {
            HttpResponseMessage response = await client.GetAsync($"https://localhost:44301/api/Projects/{id}");
            string body = await response.Content.ReadAsStringAsync();

            return JsonConvert.DeserializeObject<Project>(body);
        }

        static async Task<List<ProjectTask>> GetTasks()
        {
            HttpResponseMessage response = await client.GetAsync("https://localhost:44301/api/Tasks");
            string body = await response.Content.ReadAsStringAsync();

            return JsonConvert.DeserializeObject<List<ProjectTask>>(body);
        }

        static async Task<ProjectTask> GetTaskById(int id)
        {
            HttpResponseMessage response = await client.GetAsync($"https://localhost:44301/api/Tasks/{id}");
            string body = await response.Content.ReadAsStringAsync();

            return JsonConvert.DeserializeObject<ProjectTask>(body);
        }

        static async Task<List<Team>> GetTeams()
        {
            HttpResponseMessage response = await client.GetAsync("https://localhost:44301/api/Teams");
            string body = await response.Content.ReadAsStringAsync();

            return JsonConvert.DeserializeObject<List<Team>>(body);
        }

        static async Task<Team> GetTeamById(int id)
        {
            HttpResponseMessage response = await client.GetAsync($"https://localhost:44301/api/Team/{id}");
            string body = await response.Content.ReadAsStringAsync();

            return JsonConvert.DeserializeObject<Team>(body);
        }

        static async Task<List<User>> GetUsers()
        {
            HttpResponseMessage response = await client.GetAsync("https://localhost:44301/api/Users");
            string body = await response.Content.ReadAsStringAsync();

            return JsonConvert.DeserializeObject<List<User>>(body);
        }

        static async Task<User> GetUserById(int id)
        {
            HttpResponseMessage response = await client.GetAsync($"https://localhost:44301/api/Users/{id}");
            string body = await response.Content.ReadAsStringAsync();

            return JsonConvert.DeserializeObject<User>(body);
        }
    }
}
