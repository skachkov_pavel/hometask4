using HomeTask4.Storage;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using Models;

namespace HomeTask4
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddControllers();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "HomeTask4", Version = "v1" });
            });
            services.AddDbContext<HomeTaskContext>(options => options.UseSqlServer(Configuration.GetConnectionString("SqlServer")));
            services.AddScoped(typeof(IRepository<>), typeof(Repository<>));
            //services.AddSingleton(typeof(IRepository<Project>), typeof(Repository<Project>));
            //services.AddSingleton(typeof(IRepository<ProjectTask>), typeof(Repository<ProjectTask>));
            //services.AddSingleton(typeof(IRepository<Team>), typeof(Repository<Team>));
            //services.AddSingleton(typeof(IRepository<User>), typeof(Repository<User>));

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "HomeTask4 v1"));
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
