﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace HomeTask4.Migrations
{
    public partial class AddSeed : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "users",
                columns: new[] { "id", "birthDay", "email", "firstName", "lastName", "registeredAt", "teamId" },
                values: new object[] { 99999, new DateTime(2001, 7, 6, 0, 8, 30, 137, DateTimeKind.Local).AddTicks(9118), "email.com", "William", "Shakespeare", new DateTime(2021, 7, 6, 0, 8, 30, 133, DateTimeKind.Local).AddTicks(7906), null });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "users",
                keyColumn: "id",
                keyValue: 99999);
        }
    }
}
