﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace HomeTask4.Migrations
{
    public partial class RenamedField : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "deadline",
                table: "projects",
                newName: "neDeadline");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "neDeadline",
                table: "projects",
                newName: "deadline");
        }
    }
}
