﻿using HomeTask4.Storage;
using Microsoft.AspNetCore.Mvc;
using Models;
using System.Threading.Tasks;

namespace HomeTask4.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IRepository<User> _repository;

        public UsersController(IRepository<User> repository)
        {
            _repository = repository;
        }
        [HttpGet]
        public async Task<ActionResult> Get()
        {
            return Ok(await _repository.ReadAll());
        }

        [HttpGet("{id}")]
        public async Task<ActionResult> Get(int id)
        {
            return Ok(await _repository.Read(id));
        }

        [HttpPost]
        public async Task<StatusCodeResult> Post([FromBody] User user)
        {
            await _repository.Create(user);
            return new StatusCodeResult(201);
        }

        [HttpPut]
        public async Task<StatusCodeResult> Put([FromBody] User user)
        {
            await _repository.Update(user);
            return Ok();
        }

        [HttpDelete]
        public StatusCodeResult Delete([FromBody] User user)
        {
            _repository.Delete(user);
            return Ok();
        }
    }
}
