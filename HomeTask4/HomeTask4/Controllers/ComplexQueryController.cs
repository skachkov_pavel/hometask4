﻿using HomeTask4.Storage;
using Microsoft.AspNetCore.Mvc;
using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HomeTask4.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ComplexQueryController : ControllerBase
    {
        private readonly IRepository<ProjectTask> _projectTaskRepository;
        private readonly IRepository<User> _userRepository;

        public ComplexQueryController(IRepository<ProjectTask> projectTaskRepository, IRepository<User> userRepository)
        {
            _projectTaskRepository = projectTaskRepository;
            _userRepository = userRepository;
        }


        [HttpGet("GetUserTasksCountPerProject/{id}")]
        public async Task<ActionResult> GetUserTasksCountPerProject(int id)
        {
            IReadOnlyList<ProjectTask> tasks = await _projectTaskRepository.ReadAll();

            return Ok(tasks
                .Where(t => t.performerId == id)
                .GroupBy(t => t.projectId)
                .ToDictionary(g => g.Key, g => g.Count()));
        }

        [HttpGet("GetUserTasksByID/{id}")]
        public async Task<ActionResult> GetUserTasksByID(int id)
        {
            IReadOnlyList<ProjectTask> allTasks = await _projectTaskRepository.ReadAll();
            return Ok(allTasks
                .Where(t => t.performerId == id && t.name.Length < 45)
                .ToList());
          
        }

        [HttpGet("GetUserFinishedTasks/{id}")]
        public async Task<ActionResult> GetUserFinishedTasks(int id)
        {
            var periodFrom = new DateTime(2021, 1, 1, 0, 0, 0);
            var periodTo = new DateTime(2021, 12, 31, 23, 59, 0);
            IReadOnlyList<ProjectTask> allTasks = await _projectTaskRepository.ReadAll();
            return Ok(allTasks
                .Where(t => t.performerId == id && t.finishedAt > periodFrom && t.finishedAt < periodTo)
                .Select(t => new TaskIdToName { Id = t.id, Name = t.name })
                .ToList());
           
        }

        [HttpGet]
        public async Task<ActionResult> GetSortedUsersWithTasks()
        {
            IReadOnlyList<ProjectTask> allProjectTasks = await _projectTaskRepository.ReadAll();
            return Ok(allProjectTasks
                .GroupBy(t => t.performerId)
                .Select(async g => 
                     new UserToTasks { UserName = (await _userRepository.Read(g.Key)).firstName, ProjectTasks = g.OrderBy(t => t.name.Length).ToList() })
                .OrderBy( async o => (await o).UserName)
                .ToList());
        }


    }
}
