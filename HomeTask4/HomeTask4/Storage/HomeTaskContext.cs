﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Models;

namespace HomeTask4.Storage
{
    public class HomeTaskContext : DbContext
    {
        public DbSet<Project> projects { get; set; }
        public DbSet<ProjectTask> projectTasks { get; set; }
        public DbSet<Team> teams { get; set; }
        public DbSet<User> users { get; set; }
        public HomeTaskContext(DbContextOptions<HomeTaskContext> dbContextOptions) : base(dbContextOptions)
        {

        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>().HasData(new User
            {
                id = 99999,
                firstName = "William",
                lastName = "Shakespeare",
                email = "email.com",
                registeredAt = DateTime.Now,
                birthDay = DateTime.Now.AddYears(-20)
                
            });

        }
    }
}
