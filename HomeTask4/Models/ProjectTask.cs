﻿using System;

namespace Models
{
    // переименовать проперти с заглавной
    public class ProjectTask : IEntity
    {
        public int id { get; set; }
        public int projectId { get; set; }
        public Project project { get; set; }
        public int performerId { get; set; }
        public User performer { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public int state { get; set; }
        public DateTime createdAt { get; set; }
        public DateTime? finishedAt { get; set; }
    }
}
